import { MockedProvider } from "@apollo/client/testing";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import { App } from "App";

describe("App component", () => {
	it("should render without crashing", () => {
		render(
			<MockedProvider>
				<MemoryRouter>
					<App />
				</MemoryRouter>
			</MockedProvider>
		);
	});
});
