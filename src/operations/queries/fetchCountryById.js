/* istanbul ignore file */
import { gql } from "@apollo/client";

import { CORE_COUNTRY_FIELDS } from "fragments";

export const FETCH_COUNTRY_BY_ID = gql`
	${CORE_COUNTRY_FIELDS}
	query FetchCountryById($_id: String) {
		Country(_id: $_id) {
			...CoreCountryFields
			alpha2Code
			alpha3Code
			area
			distanceToOtherCountries(first: 5) {
				countryName
				distanceInKm
			}
			population
			topLevelDomains {
				_id
				name
			}
		}
	}
`;
