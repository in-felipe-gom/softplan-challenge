/* istanbul ignore file */
import { gql } from "@apollo/client";

import { CORE_COUNTRY_FIELDS } from "fragments";

export const FETCH_COUNTRIES = gql`
	${CORE_COUNTRY_FIELDS}
	query FetchCountries($filter: String) {
		Country(filter: { name_starts_with: $filter }) {
			...CoreCountryFields
			_id
		}
	}
`;
