/* istanbul ignore file */
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import { ChakraProvider } from "@chakra-ui/react";
import { StrictMode } from "react";
import { render } from "react-dom";
import { BrowserRouter } from "react-router-dom";

import { App } from "App";

const client = new ApolloClient({
	cache: new InMemoryCache(),
	uri: "http://testefront.dev.softplan.com.br/",
});

render(
	<StrictMode>
		<ChakraProvider>
			<BrowserRouter>
				<ApolloProvider client={client}>
					<App />
				</ApolloProvider>
			</BrowserRouter>
		</ChakraProvider>
	</StrictMode>,
	document.getElementById("root")
);
