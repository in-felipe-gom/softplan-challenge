import { gql } from "@apollo/client";

export const CORE_COUNTRY_FIELDS = gql`
	fragment CoreCountryFields on Country {
		capital
		flag {
			svgFile
		}
		location {
			latitude
			longitude
		}
		name
	}
`;
