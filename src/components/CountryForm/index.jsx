import { Box, Button, Text } from "@chakra-ui/react";
import { vestResolver } from "@hookform/resolvers/vest";
import { useFieldArray, useForm } from "react-hook-form";
import vest, { enforce, test } from "vest";

import { Form } from "components/Form";
import { Heroicon } from "components/Heroicon";
import { Input } from "components/Input";

/* istanbul ignore next */
const validationSuite = vest.create((formData = {}) => {
	test("name", "Nome é um campo obrigatório.", () => {
		enforce(formData.name).isNotEmpty();
	});

	test("population", "População não pode ser negativa.", () => {
		enforce(formData.population).isPositive();
	});

	test("area", "Área é um campo obrigatório.", () => {
		enforce(formData.area).isNotEmpty();
	});

	test("area", "Área não pode ser negativa.", () => {
		enforce(formData.area).isPositive();
	});

	if (Array.isArray(formData.topLevelDomains)) {
		formData.topLevelDomains.forEach((topLevelDomain, index) => {
			test(
				`topLevelDomains.${index}.name`,
				"Nome de domínio de topo é um campo obrigatório.",
				() => {
					enforce(topLevelDomain.name).isNotEmpty();
				}
			);
		});
	}
});

export const CountryForm = ({
	defaultValues = {},
	onSubmit,
	...otherProps
}) => {
	const {
		control,
		formState: { errors },
		handleSubmit,
		register,
	} = useForm({
		defaultValues,
		resolver: vestResolver(validationSuite),
	});
	const { append, fields, remove } = useFieldArray({
		control,
		name: "topLevelDomains",
	});

	return (
		<Form {...otherProps} onSubmit={handleSubmit(onSubmit)}>
			<Input
				{...register("name")}
				errorMessage={errors.name?.message}
				label="Nome"
			/>
			<Input
				{...register("capital")}
				errorMessage={errors.capital?.message}
				label="Capital (opcional)"
			/>
			<Input
				{...register("area", {
					valueAsNumber: true,
				})}
				errorMessage={errors.area?.message}
				label="Área"
			/>
			<Input
				{...register("population", {
					valueAsNumber: true,
				})}
				errorMessage={errors.population?.message}
				label="População (opcional)"
			/>
			<Box width="full">
				<Text fontWeight="medium" marginBottom={2}>
					Domínios de topo
				</Text>
				{fields.map((field, index) => {
					return (
						<Box key={field.id} marginBottom={2}>
							<Input
								{...register(`topLevelDomains.${index}.name`, {
									required: true,
								})}
								defaultValue={field.name}
								errorMessage={
									(errors.topLevelDomains || {})[index]?.name.message
								}
								rightElement={{
									children: (
										<Button
											type="button"
											aria-label="Remover campo"
											onClick={() => {
												remove(index);
											}}
											data-testid="removeFieldBtn"
										>
											<Heroicon icon={Heroicon.icon.X} />
										</Button>
									),
								}}
								data-testid="tldField"
							/>
						</Box>
					);
				})}
				<Button
					colorScheme="blue"
					display="block"
					marginBottom={4}
					marginInlineStart="auto"
					type="button"
					aria-label="Adicionar novo campo"
					onClick={() => {
						append({
							name: "",
						});
					}}
					data-testid="addNewFieldBtn"
				>
					<Heroicon icon={Heroicon.icon.PLUS} />
				</Button>
			</Box>
		</Form>
	);
};
