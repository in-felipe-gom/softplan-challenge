import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import { CountryForm } from "components/CountryForm";

describe("CountryForm component", () => {
	it("should render without crashing", () => {
		render(<CountryForm />);
	});

	it("should add new field", () => {
		const { getByTestId, getAllByTestId } = render(<CountryForm />);

		userEvent.click(getByTestId("addNewFieldBtn"));
		userEvent.click(getByTestId("addNewFieldBtn"));
		userEvent.click(getByTestId("addNewFieldBtn"));

		expect(getAllByTestId("tldField").length).toBe(3);
	});

	it("should remove field", () => {
		const { getByTestId, getAllByTestId } = render(<CountryForm />);

		userEvent.click(getByTestId("addNewFieldBtn"));
		userEvent.click(getByTestId("addNewFieldBtn"));
		userEvent.click(getByTestId("addNewFieldBtn"));
		userEvent.click(getAllByTestId("removeFieldBtn")[0]);

		expect(getAllByTestId("tldField").length).toBe(2);
	});
});
