import { VStack } from "@chakra-ui/react";

export const Form = (props) => {
	return <VStack {...props} as="form" noValidate spacing={4} />;
};
