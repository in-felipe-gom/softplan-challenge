import { render, waitFor } from "@testing-library/react";

import { MapChart } from "components/MapChart";

describe("MapChart component", () => {
	it("should render without crashing", async () => {
		const alpha2Code = "BR";
		const alpha3Code = "BRA";

		const { getByTestId } = render(
			<MapChart alpha2Code={alpha2Code} alpha3Code={alpha3Code} />
		);

		await waitFor(() => {
			expect(
				getByTestId(`${alpha2Code + alpha3Code}geography`)
			).toBeInTheDocument();
		});
	});

	it("should render without crashing when closestCountries is passed", () => {
		const closestCountries = [
			{
				distanceInKm: 100,
				location: {
					latitude: 10,
					longitude: 10,
				},
				name: "Brazil",
			},
		];

		render(<MapChart closestCountries={closestCountries} />);
	});
});
