import {
	ComposableMap,
	Geographies,
	Geography,
	Line,
	Marker,
	ZoomableGroup,
} from "react-simple-maps";

const selectedCountryStyle = {
	fill: "#FF5533",
	outline: "none",
};

const unselectedCountryStyle = {
	outline: "none",
};

const textStyle = {
	fill: "#5D5D5D",
	fontFamily: "system-ui",
};

export const MapChart = ({
	alpha2Code,
	alpha3Code,
	closestCountries = [],
	location = {},
}) => {
	const selectedCountryLatitude = location.latitude;
	const selectedCountryLongitude = location.longitude;

	return (
		<ComposableMap
			projection="geoAzimuthalEqualArea"
			projectionConfig={{
				rotate: [
					selectedCountryLongitude * -1,
					selectedCountryLatitude * -1,
					0,
				],
				scale: 500,
			}}
			style={{
				border: "2px solid #D7D7D7",
				borderRadius: "6px",
			}}
		>
			<ZoomableGroup zoom={1}>
				<Geographies geography="https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json">
					{({ geographies }) => {
						return geographies.map((geography) => {
							const { ISO_A2, ISO_A3 } = geography.properties;
							const isSelected = alpha2Code === ISO_A2 || alpha3Code === ISO_A3;
							const style = isSelected
								? selectedCountryStyle
								: unselectedCountryStyle;

							return (
								<Geography
									key={geography.rsmKey}
									fill="#EAEAEA"
									geography={geography}
									stroke="#D7D7D7"
									style={{
										default: style,
										hover: style,
										pressed: style,
									}}
									data-testid={
										isSelected ? `${alpha2Code + alpha3Code}geography` : ""
									}
								/>
							);
						});
					}}
				</Geographies>
				{closestCountries.map(({ distanceInKm, location, name }) => {
					return (
						<g key={name} data-testid={name}>
							<Line
								from={[selectedCountryLongitude, selectedCountryLatitude]}
								stroke="#9999FF"
								strokeLinecap="round"
								strokeWidth={2}
								to={[location.longitude, location.latitude]}
							/>
							<Marker coordinates={[location.longitude, location.latitude]}>
								<circle fill="#FF0000" r={5} stroke="#5D5D5D" strokeWidth={2} />
								<text style={textStyle} textAnchor="middle" y={-15}>
									{name}
								</text>
								<text style={textStyle} textAnchor="middle" y={25}>
									{Math.round(distanceInKm)} km
								</text>
							</Marker>
						</g>
					);
				})}
			</ZoomableGroup>
		</ComposableMap>
	);
};

export default MapChart;
