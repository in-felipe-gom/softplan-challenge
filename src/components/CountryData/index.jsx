import { Badge, Heading, HStack, Image, Text } from "@chakra-ui/react";

import { MapChart } from "components/MapChart";

export const CountryData = ({
	alpha2Code,
	alpha3Code,
	area,
	capital,
	closestCountries = [],
	flag,
	location = {},
	name,
	population,
	topLevelDomains = [],
}) => {
	return (
		<>
			<Image
				alt={`${name} flag.`}
				htmlWidth="96"
				htmlHeight="64"
				src={flag}
				width={24}
				height={16}
			/>
			<div>
				<Heading size="md">{name}</Heading>
				<Text color="gray.500">{capital}</Text>
			</div>
			<Text>
				Área de {new Intl.NumberFormat("pt-BR").format(area)} km², com população
				de {new Intl.NumberFormat("pt-BR").format(population)} habitantes.
			</Text>
			<div>
				<Text fontWeight="bold" marginBottom={2}>
					Domínios de topo
				</Text>
				<HStack spacing={2}>
					{topLevelDomains.map(({ _id, name }) => {
						return (
							<Badge
								key={name}
								borderRadius="full"
								colorScheme="blue"
								paddingX={2}
								paddingY={0.5}
								data-testid={`${name}Tld`}
							>
								{name}
							</Badge>
						);
					})}
				</HStack>
			</div>
			<MapChart
				alpha2Code={alpha2Code}
				alpha3Code={alpha3Code}
				closestCountries={closestCountries}
				location={location}
			/>
		</>
	);
};
