import { render } from "@testing-library/react";

import { CountryData } from "components/CountryData";

describe("CountryData component", () => {
	it("should render without crashing", () => {
		render(<CountryData />);
	});

	it("should render top-level domains", () => {
		const topLevelDomains = [
			{
				_id: 1,
				name: ".br",
			},
			{
				_id: 2,
				name: ".brbr",
			},
		];

		const { getByTestId } = render(
			<CountryData topLevelDomains={topLevelDomains} />
		);

		expect(getByTestId(`${topLevelDomains[0].name}Tld`)).toBeInTheDocument();
		expect(getByTestId(`${topLevelDomains[1].name}Tld`)).toBeInTheDocument();
	});
});
