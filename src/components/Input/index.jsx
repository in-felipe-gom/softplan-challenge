import {
	FormControl,
	FormErrorMessage,
	FormHelperText,
	FormLabel,
	Input as ChakraInput,
	InputGroup,
	InputLeftAddon,
	InputLeftElement,
	InputRightAddon,
	InputRightElement,
} from "@chakra-ui/react";
import { useId } from "@reach/auto-id";
import { forwardRef } from "react";

import { Heroicon } from "components/Heroicon";

export const Input = forwardRef(function Input(
	{
		id,
		errorMessage,
		helperText,
		isDisabled,
		isReadOnly,
		isRequired,
		label,
		leftAddon = {},
		leftElement = {},
		rightAddon = {},
		rightElement = {},
		size = "md",
		...otherProps
	},
	forwardedRef
) {
	const autoId = useId(id);

	const isInvalid = otherProps.isInvalid || !!errorMessage;
	const leftDecoration = leftAddon.children ? (
		<InputLeftAddon {...leftAddon} data-testid="leftDecoration" />
	) : (
		leftElement.children && (
			<InputLeftElement {...leftElement} data-testid="leftDecoration" />
		)
	);
	const rightDecoration = rightAddon.children ? (
		<InputRightAddon {...rightAddon} data-testid="rightDecoration" />
	) : (
		rightElement.children && (
			<InputRightElement {...rightElement} data-testid="rightDecoration" />
		)
	);

	return (
		<FormControl
			id={autoId}
			isDisabled={isDisabled}
			isInvalid={isInvalid}
			isReadOnly={isReadOnly}
			isRequired={isRequired}
			size={size}
		>
			{label && <FormLabel>{label}</FormLabel>}
			<InputGroup>
				{leftDecoration}
				<ChakraInput {...otherProps} ref={forwardedRef} />
				{rightDecoration ||
					(isInvalid && (
						<InputRightElement
							children={
								<Heroicon
									color="red.500"
									icon={Heroicon.icon.EXCLAMATION_CIRCLE}
								/>
							}
						/>
					))}
			</InputGroup>
			{isInvalid ? (
				<FormErrorMessage data-testid="errorMessage">
					{errorMessage}
				</FormErrorMessage>
			) : (
				helperText && (
					<FormHelperText data-testid="helperText">{helperText}</FormHelperText>
				)
			)}
		</FormControl>
	);
});
