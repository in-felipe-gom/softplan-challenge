import { render } from "@testing-library/react";

import { Input } from "components/Input";

describe("Input component", () => {
	it("should render without crashing", () => {
		render(<Input />);
	});

	it("should render label", () => {
		const label = "Label";
		const { getByLabelText } = render(<Input label={label} />);

		expect(getByLabelText(label)).toBeInTheDocument();
	});

	it("should render addons", () => {
		const { getByTestId } = render(
			<Input leftAddon={{ children: "." }} rightAddon={{ children: "." }} />
		);

		expect(getByTestId("leftDecoration")).toBeInTheDocument();
		expect(getByTestId("rightDecoration")).toBeInTheDocument();
	});

	it("should render elements", () => {
		const { getByTestId } = render(
			<Input leftElement={{ children: "." }} rightElement={{ children: "." }} />
		);

		expect(getByTestId("leftDecoration")).toBeInTheDocument();
		expect(getByTestId("rightDecoration")).toBeInTheDocument();
	});

	it("should render error message", () => {
		const { getByTestId } = render(<Input errorMessage="Error message" />);

		expect(getByTestId("errorMessage")).toBeInTheDocument();
	});

	it("should render helper text", () => {
		const { getByTestId } = render(<Input helperText="Helper text" />);

		expect(getByTestId("helperText")).toBeInTheDocument();
	});
});
