import { Button } from "@chakra-ui/react";
import { useForm } from "react-hook-form";

import { Form } from "components/Form";
import { Heroicon } from "components/Heroicon";
import { Input } from "components/Input";

export const SearchForm = ({ onSubmit }) => {
	const { handleSubmit, register } = useForm();

	return (
		<Form width="full" onSubmit={handleSubmit(onSubmit)}>
			<Input
				{...register("filter")}
				rightElement={{
					children: (
						<Button aria-label="Buscar" type="submit">
							<Heroicon icon={Heroicon.icon.SEARCH} />
						</Button>
					),
				}}
			/>
		</Form>
	);
};
