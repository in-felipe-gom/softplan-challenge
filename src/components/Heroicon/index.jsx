import { Icon as ChakraIcon } from "@chakra-ui/react";

const HeroiconIcon = {
	EXCLAMATION_CIRCLE: "exclamationCircle",
	PLUS: "plus",
	QUESTION_MARK_CIRCLE: "questionMarkCircle",
	SEARCH: "search",
	X: "x",
};

const HeroiconFill = {
	OUTLINE: "outline",
	SOLID: "solid",
};

const icons = {
	[HeroiconIcon.EXCLAMATION_CIRCLE]: {
		[HeroiconFill.OUTLINE]: [
			"M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z",
		],
		[HeroiconFill.SOLID]: [
			"M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z",
		],
	},
	[HeroiconIcon.PLUS]: {
		[HeroiconFill.OUTLINE]: ["M12 6v6m0 0v6m0-6h6m-6 0H6"],
		[HeroiconFill.SOLID]: [
			"M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z",
		],
	},
	[HeroiconIcon.QUESTION_MARK_CIRCLE]: {
		[HeroiconFill.OUTLINE]: [
			"M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z",
		],
		[HeroiconFill.SOLID]: [
			"M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z",
		],
	},
	[HeroiconIcon.SEARCH]: {
		[HeroiconFill.OUTLINE]: ["M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"],
		[HeroiconFill.SOLID]: [
			"M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z",
		],
	},
	[HeroiconIcon.X]: {
		[HeroiconFill.OUTLINE]: ["M6 18L18 6M6 6l12 12"],
		[HeroiconFill.SOLID]: [
			"M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z",
		],
	},
};

export const Heroicon = ({
	boxSize,
	fill = HeroiconFill.SOLID,
	icon = HeroiconIcon.QUESTION_MARK_CIRCLE,
	...otherProps
}) => {
	const isSolid = fill === HeroiconFill.SOLID;
	const pathProps = isSolid
		? {
				clipRule: "evenodd",
				fillRule: "evenodd",
		  }
		: {
				strokeLinecap: "round",
				strokeLinejoin: "round",
				strokeWidth: "2",
		  };
	const svgProps = isSolid
		? {
				fill: "currentColor",
				stroke: "none",
				viewBox: "0 0 20 20",
		  }
		: {
				fill: "none",
				stroke: "currentColor",
				viewBox: "0 0 24 24",
		  };
	const paths = icons[icon][fill];

	return (
		<ChakraIcon
			{...otherProps}
			{...svgProps}
			boxSize={boxSize ? boxSize : isSolid ? "5" : "6"}
			xmlns="http://www.w3.org/2000/svg"
			aria-hidden
		>
			{paths.map((path) => {
				return <path {...pathProps} key={path} d={path} />;
			})}
		</ChakraIcon>
	);
};

Heroicon.fill = HeroiconFill;
Heroicon.icon = HeroiconIcon;
