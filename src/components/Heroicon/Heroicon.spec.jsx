import { render } from "@testing-library/react";

import { Heroicon } from "components/Heroicon";

describe("Heroicon component", () => {
	it("should render without crashing - OUTLINE", () => {
		render(<Heroicon fill={Heroicon.fill.OUTLINE} />);
	});

	it("should render without crashing - SOLID", () => {
		render(<Heroicon fill={Heroicon.fill.SOLID} />);
	});

	it("should render without crashing when boxSize is passed", () => {
		render(<Heroicon boxSize={10} />);
	});

	it("should render without crashing when icon is passed", () => {
		render(<Heroicon icon={Heroicon.icon.X} />);
	});
});
