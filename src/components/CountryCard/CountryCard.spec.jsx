import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import { CountryCard } from "components/CountryCard";

describe("CountryCard component", () => {
	it("should render without crashing", () => {
		render(
			<MemoryRouter>
				<CountryCard />
			</MemoryRouter>
		);
	});
});
