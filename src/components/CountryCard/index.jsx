import {
	Heading,
	HStack,
	Image,
	LinkBox,
	LinkOverlay,
	Text,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";

export const CountryCard = ({ id, capital, flag, name }) => {
	return (
		<LinkBox
			borderRadius="md"
			borderWidth="2px"
			height={20}
			padding={4}
			width="full"
		>
			<LinkOverlay as={Link} to={`/${id}`}>
				<HStack spacing={4}>
					<Image
						alignSelf="flex-start"
						alt={`${name} flag.`}
						htmlWidth="48"
						htmlHeight="32"
						src={flag}
						width={12}
						height={8}
					/>
					<div>
						<Heading size="md">{name}</Heading>
						<Text color="gray.500">{capital}</Text>
					</div>
				</HStack>
			</LinkOverlay>
		</LinkBox>
	);
};
