import { makeVar } from "@apollo/client";

const countriesVar = makeVar(
	JSON.parse(window.localStorage.getItem("countries")) || []
);

export const countriesStorage = {
	findById(_id) {
		return countriesVar().find((country) => country._id === _id);
	},
	getAll() {
		return countriesVar();
	},
	updateData(_id, data) {
		const countries = countriesVar();
		const index = countries.findIndex((country) => {
			return country._id === _id;
		});

		if (index !== -1) {
			const country = countries[index];

			countries[index] = { ...country, ...data };
		} else {
			countries.push({ _id, ...data });
		}

		countriesVar(countries);
		window.localStorage.setItem("countries", JSON.stringify(countriesVar()));
	},
};
