import { Container } from "@chakra-ui/react";
import { Route, Switch } from "react-router-dom";

import { CountriesPage } from "pages/CountriesPage";
import { CountryPage } from "pages/CountryPage";

export const App = () => {
	return (
		<Container data-testid="app" paddingY={4}>
			<Switch>
				<Route exact path="/">
					<CountriesPage />
				</Route>
				<Route exact path="/:_id">
					<CountryPage />
				</Route>
			</Switch>
		</Container>
	);
};
