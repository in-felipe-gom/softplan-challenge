/* istanbul ignore file */
import { useQuery } from "@apollo/client";
import { Button, VStack } from "@chakra-ui/react";
import { useState } from "react";

import { CountryCard } from "components/CountryCard";
import { SearchForm } from "components/SearchForm";
import { FETCH_COUNTRIES } from "operations/queries/fetchCountries";
import { countriesStorage } from "storage";

export const CountriesPage = () => {
	const [filter, setFilter] = useState("");
	const { data, error, fetchMore, loading, refetch } = useQuery(
		FETCH_COUNTRIES,
		{
			variables: {
				filter,
			},
		}
	);

	if (error) {
		return <p>Algo deu errado! Por favor, tente novamente...</p>;
	}

	if (loading) {
		return <p>Carregando...</p>;
	}

	const countries = data.Country.map((country) => {
		const countryFromStorage = countriesStorage.findById(country._id);

		return countryFromStorage ? { ...country, ...countryFromStorage } : country;
	});

	return (
		<VStack spacing={4}>
			<SearchForm
				onSubmit={(formData) => {
					setFilter(formData.filter);
					refetch();
				}}
			/>
			{countries.map(({ _id, capital, flag, name }) => {
				return (
					<CountryCard
						id={_id}
						key={_id}
						capital={capital}
						flag={flag.svgFile}
						name={name}
					/>
				);
			})}
			<Button
				colorScheme="blue"
				display="block"
				marginX="auto"
				onClick={() => {
					fetchMore({
						variables: {
							offset: data.Country.length,
						},
					});
				}}
			>
				Buscar mais
			</Button>
		</VStack>
	);
};
