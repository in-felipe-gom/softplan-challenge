/* istanbul ignore file */
import { useQuery } from "@apollo/client";
import { Button, HStack, Link as ChakraLink, VStack } from "@chakra-ui/react";
import { useState } from "react";
import { Link, useParams } from "react-router-dom";

import { CountryData } from "components/CountryData";
import { CountryForm } from "components/CountryForm";
import { FETCH_COUNTRIES } from "operations/queries/fetchCountries";
import { FETCH_COUNTRY_BY_ID } from "operations/queries/fetchCountryById";
import { countriesStorage } from "storage";

const formId = "countryForm";

export const CountryPage = () => {
	const { _id } = useParams();
	const countryFromStorage = countriesStorage.findById(_id);
	const countries = useQuery(FETCH_COUNTRIES);
	const { data, error, loading } = useQuery(FETCH_COUNTRY_BY_ID, {
		skip: !!countryFromStorage,
		variables: { _id },
	});
	const [editing, setEditing] = useState();

	if (error || countries.error) {
		return <p>Algo deu errado! Por favor, tente novamente...</p>;
	}

	if (loading || countries.loading) {
		return <p>Carregando...</p>;
	}

	const country = countryFromStorage || data.Country[0];
	const closestCountriesData = country.distanceToOtherCountries.map((data) => {
		return data;
	});
	const closestCountries = countries.data.Country.filter(({ name }) => {
		return closestCountriesData.some(({ countryName }) => {
			return countryName === name;
		});
	}).map((country, index) => {
		return {
			...country,
			...closestCountriesData[index],
		};
	});
	const defaultValues = {
		...country,
		topLevelDomains: country.topLevelDomains.map(({ name }) => {
			return {
				name,
			};
		}),
	};

	console.log("country:", country);
	console.log("closestCountriesData:", closestCountriesData);
	console.log("countries.data.Country:", countries.data.Country);
	console.log("closestCountries:", closestCountries);

	return (
		<>
			<ChakraLink
				as={Link}
				color="blue.500"
				display="block"
				marginBottom={4}
				to="/"
			>
				Voltar para listagem
			</ChakraLink>
			{editing ? (
				<>
					<CountryForm
						id={formId}
						defaultValues={defaultValues}
						onSubmit={(formData) => {
							console.log(formData);
							countriesStorage.updateData(_id, formData);
							setEditing(false);
						}}
					/>
					<HStack justify="flex-end" spacing={4}>
						<Button
							colorScheme="blue"
							variant="ghost"
							onClick={() => {
								setEditing(false);
							}}
						>
							Cancelar
						</Button>
						<Button colorScheme="blue" form={formId} type="submit">
							Atualizar informações
						</Button>
					</HStack>
				</>
			) : (
				<VStack align="flex-start" spacing={4}>
					<CountryData
						{...country}
						closestCountries={closestCountries}
						flag={country.flag.svgFile}
					/>
					<Button
						colorScheme="blue"
						onClick={() => {
							setEditing(true);
						}}
					>
						Editar informações
					</Button>
				</VStack>
			)}
		</>
	);
};
